<?php


class cp_layout {
	public $template;
	public $domain;
	private $elements;
	private $needs_save;
	public $just_created;

	function cp_layout($template="", $domain="") {
		if($template == "") {
			return false;
		}

		$this->needs_save = false;
		$this->elements = array();
		$this->just_created = false;

		return $this->_find_or_create($template, $domain);

	}

	public function add($element = null, $save=false) {

		if($element != null && get_class($element) == "stdClass") {

			if(!isset($element->con)) {
				$element->con = "";
			}

			if(!isset($element->width)) {
				$element->width = $element->max;
			}

			if(!isset($element->weight)) {
				$element->weight = count($this->elements);
			}

			if(!isset($element->title)) {
				$element->title = "";
			}

			if(!isset($element->enabled)) {
				$element->enabled = true;
			}

			foreach($this->elements as $k => $e) {
				if($e->id == $element->id && $e->con == $element->con) {
					return false;
				}
			}

			array_push($this->elements, $element);
			$this->needs_save = true;

			if($save) {
				$this->save();
			}

			return true;
		}
		return false;
	}

	public function remove($id="", $con="") {
		foreach($this->elements as $k => $e) {
			if(
				$e->id == $id &&
				$e->con == $con
			) {
				unset($this->elements[$k]);
				$this->elements = array_values($this->elements);
				$this->needs_save = true;
				return true;
			}
		}

		return false;
	}

	public function update($element = null, $save = true) {

		if($element != null && get_class($element) == "stdClass") {

			if(!isset($element->con)) {
				$element->con = "";
			}

			if(!isset($element->width)) {
				$element->width = $element->max;
			}

			if(!isset($element->title)) {
				$element->title = "";
			}

			foreach($this->elements as $k => $e) {
				if($e->id == $element->id && $e->con == $element->con) {
					if(!($element == $e)) {
						$this->elements[$k] = $element;
						$this->needs_save = true;

						if($save) {
							$this->save();
						}
						return true;
					} else {
						return false;
					}
				}
			}
		}
		return false;
	}

	public function find($id = "", $con = "") {
		foreach($this->elements as $k => $e) {
			if($e->id == $id && $e->con == $con) {
				return clone $e;
			}
		}
	}

	private function _find_or_create($template="", $domain="") {
		if(!$this->_find($template, $domain)) {
			return $this->_create($template, $domain);
		} else {
			return true;
		}
	}

	private function _find($template="", $domain=NULL) {

		if($domain != NULL) {
			$result = db_query(
				'SELECT
					cpl.elements
					FROM {custompage_layouter} cpl
					WHERE cpl.template = :tid AND cpl.domain = :did',
				array(":tid" => $template, ":did" => $domain)
				);
		} else {
			$result = db_query(
				'SELECT
					cpl.elements
					FROM {custompage_layouter} cpl
					WHERE cpl.template = :tid',
				array(":tid" => $template)
				);
		}

		if($result->rowCount() == 1) {
			$this->elements = (array)unserialize($result->fetchColumn(0));
			$this->template = $template;
			$this->domain = $domain;
			return true;
		}

		return false;
	}

	private function _create($template="", $domain=NULL) {

		$insert_obj = array(
			'template' => $template,
			'elements' => serialize($this->elements),
			);

		if($domain != NULL) {
			$insert_obj['domain'] = $domain;
		}

		$result = db_insert("custompage_layouter")
			->fields($insert_obj)
			->execute();

		$this->template = $template;
		$this->domain = $domain;
		$this->just_created = true;
		return true;
	}

	public function save() {

		if($this->needs_save) {

			if($this->domain != NULL) {
				$result = db_update("custompage_layouter")
					->fields(array(
						'elements' => serialize($this->elements),
						))
					->condition('template', $this->template)
					->condition('domain', $this->domain)
					->execute();
			} else {
				$result = db_update("custompage_layouter")
					->fields(array(
						'elements' => serialize($this->elements),
						))
					->condition('template', $this->template)
					->execute();
			}

			if($result) {
				$this->needs_save = false;
				return true;
			}
		}

		return false;
	}

	public function elements() {

		usort($this->elements, function($a, $b) {
    	return $a->weight > $b->weight ? 1 : -1;
    });

		return $this->elements;
	}
}
