

jQuery(document).ready(function(){

	jQuery(".custompage_layouter_actions .save").click(save);
	jQuery(".custompage_layouter_actions .exit").click(exit);

	jQuery("div.cplsortable.custompage_layouter_hidden").each(function(){
		disable_element(jQuery(this).attr("id"), true);
	});

	jQuery(".custompage_layouter_actions select.add").change(function(){
		enable_element(jQuery(this).val());
	});

	jQuery("div.cplsortable a.disable").click(function(){
		disable_element(jQuery(this).parent().attr("id"));
	});

	jQuery(".custompage_layouter_actions").each(function(){
		disable_actions(jQuery(this), true);
	});

});

function enable_actions(obj) {

	save = obj.find(".save");
	exit = obj.find(".exit");

	save.show('drop', {direction: 'left'}, 100);
	exit.show('drop', {direction: 'left'}, 100);
}

function disable_actions(obj, init) {

	save = obj.find(".save");
	exit = obj.find(".exit");

	if(init == undefined) {
		save.hide('drop', {direction: 'left'}, 100);
		exit.hide('drop', {direction: 'left'}, 100);
	} else {
		save.hide();
		exit.hide();
	}
}

function custompage_layouter_change_callback(event, ui) {
	enable_actions(jQuery(this).parent().find(".custompage_layouter_actions"));
}

function custompage_layouter_start_resize_callback(event, ui) {
	// Save current width
	data = jQuery(this).data("custompage_layouter");
	data.current_width = jQuery(this).width();
	min = data.min;
	max = data.max;
	width = data.width;

	grid_one = data.current_width / width;

	// Set jQuery UI Resite min + max
	jQuery(this).resizable('option', 'minWidth', min * grid_one);
	jQuery(this).resizable('option', 'maxWidth', max * grid_one);

	jQuery(this).data("custompage_layouter", data);
}

function custompage_layouter_resize_callback(event, ui) {


	data = jQuery(this).data("custompage_layouter");
	new_width = jQuery(this).width();
	old_width = data.current_width;
	min = data.min;
	max = data.max;
	width = data.width;

	grid_one = old_width / width;
	width = parseInt(new_width / grid_one)+1;

	if(width > max) width = max;
	if(width < min) width = min;

	prev_width = parseInt(old_width / grid_one);
	data.width = width;
	data.current_width = null;

	jQuery(this).data("custompage_layouter", data);
	//update_object([data], data.template, data.domain);

	// Remove old grid class
	jQuery(this).removeClass(data.grid_class_prefix + prev_width);

	// Add new grid class
	jQuery(this).addClass(data.grid_class_prefix + width);

	jQuery(this).removeAttr("style");
	enable_actions(jQuery(this).parent().find(".custompage_layouter_actions"));
}


function update_object(data, template, domain, creator) {
	jQuery.post(Drupal.settings.basePath + 'admin/structure/custompage/layouter_update', {
		template: 	template,
		domain: 		domain,
		data: 			data,
	}, function(data, textStatus, jqXHR){

		if(!data.success) {
			alert(data.message);
		} else {
			console.log(creator);
			disable_actions(creator);
		}

	}, 'json');
}

function disable_element(id, init) {
	element = jQuery("#" + id);

	element.data("custompage_layouter").enabled = false;
	element.fadeOut("fast").hide();
	element.addClass("custompage_layouter_hidden");
	title = element.data("custompage_layouter").title;
	
	if(title == "") {
  	title = element.data("custompage_layouter").id;
	}
	
	element.parent().find(".custompage_layouter_actions select.add").append('<option value="' + id + '">' + title + '</option>');

	if(init == undefined) {
		enable_actions(element.parent().find(".custompage_layouter_actions"));
	}
}

function enable_element(id) {
	element = jQuery("#" + id);
	
	element.removeClass("custompage_layouter_hidden");
	element.data("custompage_layouter").enabled = true;
	element.show();
	element.parent().find(".custompage_layouter_actions select.add option[value=" + id + "]").remove();
	enable_actions(element.parent().find(".custompage_layouter_actions"));
}

function save() {

	//get all elements
	data = [];
	template = "";
	domain = "";

	counter = 0;

	jQuery(this).parent().parent().parent().find("div.cplsortable").each(function(){
		data[counter] = jQuery(this).data("custompage_layouter");
		data[counter].weight = counter;

		if(template == "") {
			template = data[counter].template;
		}

		if(domain == "") {
			domain = data[counter].domain;
		}

		counter++;
	});

	//post them to the ajax endpoint
	update_object(data, template, domain, jQuery(this).parent().parent());

}

function exit() {
	window.location.reload();
}







